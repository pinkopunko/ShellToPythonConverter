#!/usr/bin/perl -w


# store the file lines into an array
@fileLines = <>;


# --------------------------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
# --------------------------------------------------------------------------------------------------------------------
# checks for if "import subprocess" has occurred once before.
# 0: it has NOT
# 1: it has.
$impSub = 0;
# note to future sam, could this be affected by the scope? if it occurs once in a function will it be globally true throughout the python function?


# checks for if "import os" has occurred once before.
# 0: it has NOT
# 1: it has.
$impOs = 0;


# checks for if "import sys" has occurred once before.
# 0: it has NOT
# 1: it has.
$impSys = 0;


# checks for if "import glob" has occurred once before.
# 0: it has NOT
# 1: it has.
$impGlob = 0;


# checks for if "import os.path" has occurred once before.
# 0: it has NOT
# 1: it has.
$impOsPath = 0;


# checks for the number of tabs we currently need.
# implies that we've entered a for/while loop
$numTab = 0;


# --------------------------------------------------------------------------------------------------------------------
# IMPORTS
# --------------------------------------------------------------------------------------------------------------------
#create a temporary Array
@tmpArray = ();

# Traverse through all the lines in the array/file
foreach $line (@fileLines){
    
    # chomp the line for trailing white spaces.
    chomp $line;
    
    # trim the beginning white spaces
    $line =~ s/^\s+//;
    
    # ------- #!\/bin\/bash/ -------
    if ( $line =~ /^#!\/bin\//){
        print "#!/usr/bin/python2.7 -u\n";
    
    
    # ------- echo -------
    } elsif ( $line =~ /^echo (.*)/ && ($impSys eq 0 || $impOs eq 0)) {
        
        # add each of the word in the line to the array
        # regexp: looks for $ (optional) and [a-zA-Z_0-9] characters
        my @wordArray = $line =~ /\$?\S+/g;
        
        # traverse through all the words in the array (i.e. the current line)
        foreach $word (@wordArray){
            
            # ---------- System Variable (e.g. $1, $2) ----------
            if ( $word =~ /^\$([0-9]+)/ ){
            
                # check IF "import sys" has not been used
                if ( $impSys eq 0 ){
                    my $tmpLine = sprintf ("import sys\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impSys to indicate to other functions "import sys" has occurred.
                    $impSys = 1;
                }
                
            # ---------- "$$" ----------
            } elsif ( $word =~ /^\$\$/ ){
                
                # check IF "import os" has not been used
                if ( $impOs eq 0 ){
                    my $tmpLine = sprintf ("import os\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impOs to indicate to other functions "import os" has occurred.
                    $impOs = 1;
                }
                
            # ---------- "$*" ----------
            } elsif ( $word =~ /^\$\*/ ){
            
                # check IF "import sys" has not been used
                if ( $impSys eq 0 ){
                    my $tmpLine = sprintf ("import sys\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impSys to indicate to other functions "import sys" has occurred.
                    $impSys = 1;
                }
                
            }
        }
    
    
    # ------- ls -------
    # ls - requires "import subprocess" function
    # ls - possibly requires "import sys" function
    } elsif ( $line =~ /^ls *(.*)/ && ($impSub eq 0 || $impSys eq 0) ){
        
        # check IF "import subprocess" has not been used
        if ( $impSub eq 0 ){
            my $tmpLine = sprintf ("import subprocess\n");
            push @tmpArray, $tmpLine;
            
            # alter impSub to indicate to other functions "import subprocess" has occurred.
            $impSub = 1;
        }
        
        # check if we're dealing with either "-las" or "@"
        # Note: not sure which one is right between the two.
        if ( $1 =~ /^\-las *(.*)/ ) {
            
            # IF the 3rd argument supplied is "@"
            if ( $1 =~ /^\"\$@\"$/ ){
                
                # check IF "import sys" has not been used
                if ( $impSys eq 0 ){
                    my $tmpLine = sprintf ("import sys\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impSys to indicate to other functions "import sys" has occurred.
                    $impSys = 1;
                }
            }
        }
    
        
    # ------- pwd, id, date -------
    # pwd, id, date - requires "import subprocess" function
    } elsif ( ($line =~ /^(pwd)/ || $line =~ /^(id)/ || $line =~ /^(date)/) && $impSub eq 0 ){
        
        # check IF "import subprocess" has not been used
        if ( $impSub eq 0 ){
            my $tmpLine = sprintf ("import subprocess\n");
            push @tmpArray, $tmpLine;
            
            # alter impSub to indicate to other functions "import subprocess" has occurred.
            $impSub = 1;
        }
        
        
    # ------- cd -------
    # cd - requires "import os" function
    # cd - requires "import subprocess" function
    } elsif ( $line =~ /^cd *(.*)/ &&  ($impOs eq 0 ||  $impSub eq 0) ){
        
        # check IF "import os" has not been used
        if ( $impOs eq 0 ){
            my $tmpLine = sprintf ("import os\n");
            push @tmpArray, $tmpLine;
            
            # alter impOs to indicate to other functions "import os" has occurred.
            $impOs = 1;
        }
        
        # check IF "import subprocess" has not been used
        if ( $impSub eq 0 ){
            my $tmpLine = sprintf ("import subprocess\n");
            push @tmpArray, $tmpLine;
            
            # alter impSub to indicate to other functions "import subprocess" has occurred.
            $impSub = 1;
        }
        
        
    # ------- exit -------
    # exit - requires "import sys" function
    } elsif ( $line =~ /^exit (.)/ &&  $impSys eq 0 ) {
        
        # check IF "import sys" has not been used
        if ( $impSys eq 0 ){
            my $tmpLine = sprintf ("import sys\n");
            push @tmpArray, $tmpLine;
            
            # alter impSys to indicate to other functions "import sys" has occurred.
            $impSys = 1;
        }
        
        
    # ------- var initialisation -------
    # var from cmd line - requires "import sys" function
    } elsif ( $line =~ /^(\w*)=(.*)/ &&  $impSys eq 0 ){
        
        
        # IF we're taking from cmd line args (e.g. example=$1)
        if ( $2 =~ /\$([0-9]+)/ ){
            
            # check IF "import sys" has not been used
            if ( $impSys eq 0 ){
                my $tmpLine = sprintf ("import sys\n");
                push @tmpArray, $tmpLine;
                
                # alter impSys to indicate to other functions "import sys" has occurred.
                $impSys = 1;
            }
        }
        
        
    # ------- for -------
    # (for c_file in *.c) - requires "import glob" function
    } elsif ( $line =~ /^for *(\w*) *in *(.*)/ &&  $impGlob eq 0 ){
        
        # IF we're checking for files (e.g. for c_file in *.c)
        if ( $2 =~ /(\*\.[a-z]{1})/ ){
            
            if ( $impGlob eq 0 ){
                my $tmpLine = sprintf ("import glob\n");
                push @tmpArray, $tmpLine;
                
                # alter impSys to indicate to other functions "import sys" has occurred.
                $impGlob = 1;
            }
        }
        
        
        
    # ------- if/elsif  -------
    } elsif (($line =~ /^if +(.*)/ || $line =~ /^elif +(.*)/) && ($impOs eq 0 || $impOsPath eq 0)){
        
        my @tempArray = ();
        # create an array of all the arguments on the "elif .... " line.
        if ( $line =~ /^elif +(.*)/ ){
            @tempArray = $1 =~ /\$?\S+/g;
            
        # create an array of all the arguments on the "if .... " line.
        } elsif ( $line =~ /^if +(.*)/ ) {
            @tempArray = $1 =~ /\$?\S+/g;
        }
        
        # traverse through ea argument on the IF line
        foreach $arg (@tempArray){
            
            # IF we "test -r $file" for file readability
            if ( $arg =~ /^\-r$/ ){
                
                # check IF "import os" has not been used
                if ( $impOs eq 0 ){
                    my $tmpLine = sprintf ("import os\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impSub to indicate to other functions "import os" has occurred.
                    $impOs = 1;
                }
                
            # ELSE IF we "test -d etc" for valid directory
            } elsif ( $arg =~ /^\-d$/ ){
                
                # check IF "import os.path" has not been used
                if ( $impOsPath eq 0 ){
                    my $tmpLine = sprintf ("import os.path\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impSub to indicate to other functions "import os" has occurred.
                    $impOsPath = 1;
                }
                
            # if [ .... ] = test
            } elsif ( $tempArray[0] =~ /^\[$/ ){
                
                # check IF "import os.path" has not been used
                if ( $impOsPath eq 0 ){
                    my $tmpLine = sprintf ("import os.path\n");
                    push @tmpArray, $tmpLine;
                    
                    # alter impSub to indicate to other functions "import os" has occurred.
                    $impOsPath = 1;
                }
            }
            
        }

    # ------- read -------
    # read - requires "import sys" function
    } elsif ( $line =~ /^read *(\S+)/ ){
        
        # check IF "import sys" has not been used
        if ( $impSys eq 0 ){
            my $tmpLine = sprintf ("import sys\n");
            push @tmpArray, $tmpLine;
            
            # alter impSys to indicate to other functions "import sys" has occurred.
            $impSys = 1;
        }
     
        
    # ------- remove file -------
    # remove -f - requires "import sys" function
    } elsif ( $line =~ /^rm *(.+)/ && $impSub eq 0 ){

        
        # IF we are removing something
        # fomat: rm -f $file
        if ( $1 =~ /\-(\w+) +\$(\S+)/ ){
        
            # check IF "import subprocess" has not been used
            if ( $impSub eq 0 ){
                my $tmpLine = sprintf ("import subprocess\n");
                push @tmpArray, $tmpLine;
                
                # alter impSub to indicate to other functions "import subprocess" has occurred.
                $impSub = 1;
            }
        
        }
    }
}







# sort the array containing all the imports
@tmpArray = sort @tmpArray;

#print the array containing all the imports
foreach $line (@tmpArray){
    print "$line";
}









    
# --------------------------------------------------------------------------------------------------------------------
# CONVERSION
# --------------------------------------------------------------------------------------------------------------------
# Traverse through all the lines in the array/file
foreach $line (@fileLines){
  
    # chomp the line for trailing white spaces.
    chomp $line;
    
    # trim the beginning white spaces
    $line =~ s/^\s+//;
    
    
    # ----------------- #!\/bin\/bash/ -----------------
    # this function is placed here to avoid tabbing.
    if ( $line =~ /^#!\/bin\//){
        # do nothing
        next;
    
    
    # ----------------- for, while -> done -----------------
    } elsif ( $line =~ /^done/ ){
        # reduce the number of tabs to be printed.
        $numTab--;
        next;
    
    
    # ----------------- for, while -> do -----------------
    # also is careful to stop avoiding "done".
    } elsif ( $line =~ /^do/ ){
        # increase the number of tabs to be printed.
        $numTab++;
        next;
        

    # ----------------- if -> elif -----------------
    } elsif ( $line =~ /^elif +(\w+) +/ ){
        # tab back once for "else"
        $numTab--;
        
        
    # ----------------- if, elif -> then -----------------
    } elsif ( $line =~ /^then/ ){
        
        $numTab++;
        next;

        
    # ----------------- if -> else -----------------
    } elsif ( $line =~ /^else/ ){
        # tab back once for "else"
        $numTab--;
        
        # ------- tabs -------
        my $tabCount = 0;
        while ( $tabCount lt $numTab ){
#            print "\t";
            print "    ";
            $tabCount++;
        }
        
        # tab ahead again.
        $numTab++;
        
        # print "else:"
        print "else\:\n";

        next;
        

    # ----------------- if -> fi -----------------
    } elsif ( $line =~ /^fi$/ ){
        # reduce the number of tabs to be printed.
        $numTab--;
        next;
    }


        
        
        
    # ----------------- tabs -----------------
    my $tabCount = 0;
    while ( $tabCount lt $numTab ){
#        print "\t";
        print "    ";
        $tabCount++;
    }
    
        
        



    # ----------------- echo -----------------
    if ( $line =~ /^echo (.*)/ || $line =~ /^die *(.*)/) {
        
        $allWords = $1;
        
        my $dieTrue = 0;
        
        # add all invidual words into an array
        my @wordArray = $1 =~ /\$?\S+/g;
        
        if ( $line =~ /^die *(.*)/){
            $dieTrue = 1;
        }
        
        # are we appending to file?
        my $appendToFile = 0;
        # this is the file we are appending to
        my $appendsTo;
        
        # traverse through words, find out how we're using echo
        foreach my $word (@wordArray){
            # ---------- >> ----------
            if ( $word =~ /^\>\>\$(\S+)/ ){
                $appendToFile = 1;
                $appendsTo = $1;
            }
        }
        
        # ---------- >> ----------
        if ( $appendToFile eq 1 ){
            
            #get the first arg
            my $output = $wordArray[0];
            
            # remove the "$" sign
            $output =~ s/^\$//;
            
            print "with open($appendsTo, 'a') as f: print >>f,  $output";
            
            
        # ---------- cmd line ----------
        } else {
        
            print "print";
            
            # ---------- single quotes ----------
            if ( $allWords =~ /^\'(.*)\' *(.*)/ ){
                print " \'$1\'";
                
            # ---------- double quotes ----------
            } elsif ( $allWords =~ /^\"(.*)\" *(.*)/ ){
                print " \"";
                
                # init a variable to help print first word with a comma at the front.
                my $count = 0;
                
                my $doubleQuote = 0;
                
                # traverse through all the words in the array (i.e. the current line)
                foreach my $word (@wordArray){
                    
                    if ( $word =~ /\"/ && $doubleQuote lt 2 ){
                        $doubleQuote++;
                    }
                    #remove the double quotes from words
                    $word =~ s/\"//;
                    
                    
                    # ---------- System Variable (e.g. $1, $2) ----------
                    if ( $word =~ /^\$([0-9]+)/ ){
                        if ( $count eq 0 ){
                            print "sys.argv[$1]";
                        } else {
                            print " sys.argv[$1]";
                        }

                    } elsif ( $word =~ /\$([\@\?]){1}/ ){
                        
                        if ( $count eq 0 ){
                            print "sys.argv[$1]";
                        } else {
                            print " sys.argv[$1]";
                        }
                        
                    # ---------- "$$" ----------
                    } elsif ( $word =~ /^\$\$/ ){
                        
                        # IF first word, we omit the space
                        if ( $count eq 0 ){
                            print "os.getpid()";
                            
                        # ELSE print with the comma
                        } else {
                            print " os.getpid()";
                        }

                    # ---------- "$#" ----------
                    } elsif ( $word =~ /^\$#/ ){
                        
                        # IF first word, we omit the space
                        if ( $count eq 0 ){
                            print "len(sys.argv[1\:])";
                            
                        # ELSE print with the space
                        } else {
                            print " len(sys.argv[1\:])";
                        }
                        
                    # ---------- "$*" ----------
                    } elsif ( $word =~ /^\$\*/ ){
                        
                        
                        # IF first word, we omit the space
                        if ( $count eq 0 ){
                            print "sys.argv[1:]";
                            
                            # ELSE print with the space
                        } else {
                            print " sys.argv[1:]";
                        }
                    
                    # ---------- Variable (i.e. a $hello) ----------
                    } elsif ( $word =~ /^\$(\w+)/ ){
                        
                        if ( $count eq 0 ){
                            print "$1";
                        } else {
                            print " $1";
                        }
                        
                    # ---------- normal word ----------
                    } else {
                        
                        # IF first word, omit the comma
                        if ( $count eq 0 ){
                            print "$word";
                            
                            # ELSE print with the comma
                        } else {
                            print " $word";
                        }

                    }
                    
                    #increment the counter
                    $count++;
                
                    if ( $doubleQuote eq 2 ){
                        last;
                    }
                }
                print "\"";
            
            # ---------- no quotes ----------
            } else {
                
                # init a variable to help print first word with a comma at the front.
                my $count = 0;
                
                # traverse through all the words in the array (i.e. the current line)
                foreach my $word (@wordArray){
                    
                    # ---------- System Variable (e.g. $1, $2) ----------
                    if ( $word =~ /^\$([0-9]+)/ ){
                        
                        # IF first word, we omit the comma
                        if ( $count eq 0 ){
                            print " sys.argv[$1]";
                            
                        # ELSE print with the comma
                        } else {
                            print ", sys.argv[$1]";
                        }
                        
                    } elsif ( $word =~ /\$([\@\?]{1})/ ){
                    
                        # IF first word, we omit the comma
                        if ( $count eq 0 ){
                            print " sys.argv[$1]";
                            
                        # ELSE print with the comma
                        } else {
                            print ", sys.argv[$1]";
                        }
                        
                    # ---------- "$$" ----------
                    } elsif ( $word =~ /^\$\$/ ){
                        
                        # IF first word, we omit the comma
                        if ( $count eq 0 ){
                            print " os.getpid()";
                            
                            # ELSE print with the comma
                        } else {
                            print ", os.getpid()";
                        }
                        
                    # ---------- "$#" ----------
                    } elsif ( $word =~ /^\$#/ ){
                    
                        # IF first word, we omit the comma
                        if ( $count eq 0 ){
                            print " len(sys.argv[1\:])";
                            
                            # ELSE print with the comma
                        } else {
                            print ", len(sys.argv[1\:])";
                        }
                    
                    # ---------- "$*" ----------
                    } elsif ( $word =~ /^\$\*/ ){
                        # IF first word, we omit the space
                        if ( $count eq 0 ){
                            print " sys.argv[1:]";
                            
                            # ELSE print with the space
                        } else {
                            print ", sys.argv[1:]";
                        }
                        
                    # ---------- Variable (i.e. a $hello) ----------
                    } elsif ( $word =~ /^\$(\w+)/ ){
                        
                        # IF first word, omit the comma
                        if ( $count eq 0 ){
                            print " $1";
                            
                        # ELSE print with the comma
                        } else {
                            print ", $1";
                        }
                        
                    # ---------- normal word ----------
                    } else {
                        
                        # IF first word, omit the comma
                        if ( $count eq 0 ){
                            print " '$word'";
                            
                        # ELSE print with the comma
                        } else {
                            print ", '$word'";
                        }
                    }
                    
                    #increment the counter
                    $count++;
                }
            }
        }
        
        if ( $dieTrue eq 1 ){
            print "\n";
            # tab back once for "die"
            $numTab--;
            
            # ------- tabs -------
            my $tabCount = 0;
            while ( $tabCount lt $numTab ){
                #            print "\t";
                print "    ";
                $tabCount++;
            }
            
            print "sys.exit(0)";
            # tab ahead again.
            $numTab++;
        }
        
        
    # ----------------- ls -----------------
    } elsif ( $line =~ /^ls *(\-?.*)/ ){
        
        # IF we're dealing with a "-l"
        if ( $1 =~ /^(\-l) +(.*)/ ) {
            
            #print the function call for ls in python
            print "subprocess.call(['ls', '$1', '$2'])";
            
            
        # ELSE IF we're dealing with a "-las"
        } elsif ( $1 =~ /^(\-las) +(.*)/ ) {
            
            # if the 3rd argument supplied is "@"
            if ( $2 =~ /^\"\$@\"$/ ){
                print "subprocess.call(['ls', '-las'] + sys.argv[1:])";
            }
        
        # ELSEIF there's no "-l" but there is a directory
        } elsif ( $1 =~ /(.+)/ ){
            
            #print the function call for ls in python
            print "subprocess.call(['ls', '$1'])";
            
        # ELSE if it's only "ls"
        } else {
            
            #print the function call for ls in python
            print "subprocess.call(['ls'])";
        }

        
    # ----------------- pwd, id, date -----------------
    } elsif ( $line =~ /^(pwd)/ || $line =~ /^(id)/ || $line =~ /^(date)/){
        
        # pwd - requires "import process" function
        print "subprocess.call(['$1'])";
      
 
    # ----------------- var initialisation -----------------
    } elsif ( $line =~ /^(\w*)=(.*)/ ){
        
        # store the variable name in a tempVar
        my $tempVar = $1;
        
        print "$tempVar =";
        
        # store the new value to be copied into the new variable
        my $newVal = $2;
        
        
        # IF we're taking from cmd line args (e.g. example=$1)
        if ( $newVal =~ /\$([0-9]+)/ ){
            
            print " sys.argv[$1]";
            
        # ELSE IF we init a variable from another variable's value (e.g. hello=$ergn)
        } elsif ( $newVal =~ /^\$(\w+)/ ){
            print " $1";
         
        # ELSE IF we use back quotes
        # OR
        # IF we have $(( ))
        } elsif ( $newVal =~ /^\`expr +(.*)\`/ || $2 =~ /^\$\(\((.*)\)\)/ ){          #`{ this stops everything below this line from going red.
            
            
            # Takes all the arguements from the RHS of "=" sign of non-whitespace value, stores into an array.
            my @tempArray = $1 =~ /\$?\S+/g;
            
            #create a counter to check for the first word "expr"
            my $count = 0;
            
            # Iterate through all the arguments
            foreach $arg (@tempArray){
                
                # ---------- variable ----------
                if ( $arg =~ /^\$(\w+)/){
                    print " int($1)";
                    
                # ---------- normal number ----------
                } elsif ( $arg =~ /^(\d+)$/ ){
                    print " $1";
                    
                # ---------- b/t single quotes ----------
                } elsif ( $arg =~ /\'(\S+)\'/ ){
                    print " $1";
                    
                # ELSE just print that argument
                } else {
                    print " $arg";
                }
                
                #iterate the counter
                $count++;
            }
            
        # ELSE IF we intialised the variable with a number
        } elsif ( $newVal =~ /(\d+)/ ){
            print " $1";
        
        # x="Small files: " etc
        } elsif ( $2 =~ /(\".*\")/ ){
            print " $1";
            
        # ELSE print for normal variable initialisation
        } else {
            # print the new format of initialisation with single apostrophes and spaces
            print " '$newVal'";
        }
        

    # ----------------- cd -----------------
    } elsif ( $line =~ /^cd *(.*)/ ){
        
        # ---------- System Variable (e.g. $1, $2) ----------
        if ( $1 =~ /^\$([0-9]+)/ ){
            my $sysArg = "sys.argv[$1]";
            print "os.chdir($sysArg)";
            
        } else {
            print "os.chdir('$1')";
        }
        
        
    # ----------------- while -----------------
    } elsif ( $line =~ /^while +(.*)/ ){
        
        print "while";
        
        # IF we do a comparison b/t 2 variables via "test"
        # OR
        # IF we do a comparison b/t 2 variables via square brackets
        if ( $1 =~ /^test +\$(\w+) +\-(\w+) +\$(\w+)/ ||  $1 =~ /^\[ *\$(\w+) +\-(\w+) +\$(\w+)/ ){
            
            #ignores "test"
            
            # initialise the variables for usage in the scope below.
            my $compVar1 = $1;
            my $compVar2 = $3;
            
            # IF comparison regexp is a NUMERIC comparison
            if ( $2 =~ /^(\w+)$/ ){
                
                print " int($compVar1)";
                
                #IF the comparison regexp is:
                if ( $1 =~ /le/ ){
                    print " <=";
                    
                } elsif ( $1 =~ /ge/ ){
                    print " >=";
                    
                } elsif ( $1 =~ /ne/ ){
                    print " !=";
                    
                } elsif ( $1 =~ /lt/ ){
                    print " <";
                    
                } elsif ( $1 =~ /gt/ ){
                    print " >";
                    
                } elsif ( $1 =~ /eq/ ){
                    print " ==";
                    
                }
                
                print " int($compVar2)";
            }
            
#            while int(number) <= int(finish):
#            while [ $number -le $finish ]
        # ELSE IF do a comparison b/t two variables through square brackets
#        } elsif ( $1 =~ /^\[ *\$(\w+) +\-(\w+) +\$(\w+)/ ){
#            
#            
#
        # ELSE IF we're dealing with one test argument (e.g. while true)
        } elsif ( $1 =~ /^(\S+)/ ){
            
            #e.g. "while true" == not subprocess.call(['true'])
            print " not subprocess.call([\'$1\'])";
            
        }
        # print the colon
        print "\:";
        
        
    # ----------------- for -----------------
    } elsif ( $line =~ /^for *(\w*) *in *(.*)/ ){
        
        # print the start of the for loop
        print "for $1 in";
        
        # IF we're checking for files (e.g. for c_file in *.c)
        if ( $2 =~ /(\*\.[a-z]{1})/ ){
            print " sorted(glob.glob(\"$1\"))";
            
        # ELSE perform the standard format for a "for loop"
        } else {
            
            # add each of the word in the line to the array
            # regexp: looks for $ (optional) and [a-zA-Z_0-9] characters
            my @wordArray = $2 =~ /\$?\S+/g;
            
            # init a variable to keep track of which position we're at. Helps with printing the first word without a comman at the front.
            my $count = 0;
            
            # traverse through all the words in the array (i.e. the current line)
            foreach $word (@wordArray){
                
                # ELSE IF we are dealing with a number and...
                if ( $word =~ /^\d*$/ ){
                    
                    # IF we're dealing with the first word, we omit the comma
                    if ( $count eq 0 ){
                        print " $word";
                        
                        # ELSE print it with the comma
                    } else {
                        print ", $word";
                    }
                    
                # ---------- System Variable (e.g. $1, $2) ----------
                } elsif ( $word =~ /^\$([0-9]+)/ ){
                    
                    # IF first word, we omit the comma
                    if ( $count eq 0 ){
                        print " sys.argv[$1]";
                        
                        # ELSE print with the comma
                    } else {
                        print ", sys.argv[$1]";
                    }
                    
                } elsif ( $word =~ /\$([\@\?]{1})/ ){
                    
                    # IF first word, we omit the comma
                    if ( $count eq 0 ){
                        print " sys.argv[$1]";
                        
                        # ELSE print with the comma
                    } else {
                        print ", sys.argv[$1]";
                    }
                    
                # ---------- "$$" ----------
                } elsif ( $word =~ /^\$\$/ ){
                    
                    # IF first word, we omit the comma
                    if ( $count eq 0 ){
                        print " os.getpid()";
                        
                        # ELSE print with the comma
                    } else {
                        print ", os.getpid()";
                    }
                    
                # ---------- "$#" ----------
                } elsif ( $word =~ /^\$#/ ){
                    
                    # IF first word, we omit the comma
                    if ( $count eq 0 ){
                        print " len(sys.argv[1\:])";
                        
                        # ELSE print with the comma
                    } else {
                        print ", len(sys.argv[1\:])";
                    }
                    
                # ---------- "$*" ----------
                } elsif ( $word =~ /^\$\*/ ){
                    # IF first word, we omit the space
                    if ( $count eq 0 ){
                        print " sys.argv[1:]";
                        
                        # ELSE print with the space
                    } else {
                        print ", sys.argv[1:]";
                    }
                    
                # ---------- Variable (i.e. a $hello) ----------
                } elsif ( $word =~ /^\$(\S+)/ ){
                    
                    # IF first word, omit the comma
                    if ( $count eq 0 ){
                        print " $1";
                        
                        # ELSE print with the comma
                    } else {
                        print ", $1";
                    }
                    
                # ---------- brace ----------
                } elsif ( $word =~ /^\$[\(]/ || $word =~ /^\)\;/ ){
                    
                    #ignore
                    #used: for i in $( ls );
                    
                # ---------- comparison regexp ----------
                } elsif ( $word =~ /^\-le$/ ){
                    print " <=";
                    
                } elsif ( $word =~ /^\-ge$/ ){
                    print " >=";
                    
                } elsif ( $word =~ /^\-ne$/ ){
                    print " !=";
                    
                } elsif ( $word =~ /^\-lt$/ ){
                    print " <";
                    
                } elsif ( $word =~ /^\-gt$/ ){
                    print " >";
                    
                } elsif ( $word =~ /^\-eq$/ || $word =~ /^\=$/ ){
                    print " ==";
                    
                } elsif ( $word =~ /^\-o$/ || $word =~ /^\|\|$/ ){
                    print " or";
                    
                } elsif ( $word =~ /^\&\&$/ || $word =~ /^\-a$/ ){
                    print " and";
                    
                # ---------- normal word ----------
                } else {
                    
                    if ( $word =~ /^ls$/){
                        print " subprocess.call(['ls']) )";
                        
                    } elsif ( $word =~ /^\)$/ ){
                        
                    
                    # IF first word, omit the comma
                    } elsif ( $count eq 0 ){
                        print " '$word'";
                        
                        # ELSE print with the comma
                    } else {
                        print ", '$word'";
                    }
                }
                
                #increment the counter
                $count++;
            }
        }
        
        # print the colon
        print "\:";
        
        
    # ----------------- if -----------------
    } elsif ( $line =~ /^if +(.*)/ || $line =~ /^elif +(.*)/ ){

        my @tempArray = ();
        
        if ( $line =~ /^elif +(.*)/ ){
            print "elif";
            # create an array of all the arguments on the "elif .... " line.
            @tempArray = $1 =~ /\$?\S+/g;
        } elsif ( $line =~ /^if +(.*)/ ) {
            print "if";
            # create an array of all the arguments on the "if .... " line.
            @tempArray = $1 =~ /\$?\S+/g;
        }

        # used to check the first arg for "test" or "fgrep" etc.
        my $count = 0;
        
        # is it a test?
        my $isTest = 0;
        # is it an fgrep?
        my $isFgrep = 0;
        
        # has " not subprocess.call(['fgrep'," been printed?
        my $fgrepPrint = 0;
        
        # -r is used
        my $isR = 0;
        # -d is used
        my $isD = 0;
        # -x is used
        my $isX = 0;
        # -q is used
        my $isQ = 0;
        
        # traverse through ea argument on the IF line
        foreach $arg (@tempArray){

            # if we run into a comment, stops
            if ( $arg =~ /^\#.*/ ){
                last;
            }
            
            # ----- Testing the type of IF case (e.g. test, fgrep) -----
            
            # if test ....
            # makes sure we check this only for the first argument to avoid strings with the same name
            if ( $tempArray[0] =~ /^test$/ && $count eq 0 ){
                $isTest = 1;
                $count++;
                next;
                
            # if fgrep ....
            # makes sure we check this only for the first argument to avoid strings with the same name
            } elsif ( $tempArray[0] =~ /^fgrep$/ && $count eq 0 ){
                $isFgrep = 1;
                $count++;
                next;
                
            # if [ .... ] = test
            } elsif ( $tempArray[0] =~ /^\[$/ && $count eq 0 ){
                $isTest = 1;
                $count++;
                next;
                
            # when we close the square brackets, we do nothin
            } elsif ( $arg =~ /^\]$/ ){
                $count++;
                next;
                
            # if .... = test
            } elsif ( $isFgrep eq 0 ) {
                $isTest = 1;
            }
            
            # ---------- "test" ----------
            if ( $isTest eq 1 ){
                
                # IF we "test -r $file" for file readability
                if ( $arg =~ /^\-r$/ ){
                    $isR = 1;
                    $count++;
                    next;
                    
                # ELSE IF we "test -d etc" for valid directory
                } elsif ( $arg =~ /^\-d$/ ){
                    $isD = 1;
                    $count++;
                    next;
                }
                
                
                # ---------- "-r" ----------
                # IF we "test -r $file" for file readability
                if ( $isR eq 1 ){
                    
                    # IF the arg is a variable with a "$" sign in front...
                    if ( $arg =~ /^\$.*/ ){
                        
                        # ---------- System Variable (e.g. $1, $2) ----------
                        if ( $arg =~ /^\$([0-9]+)/ ){
                            $arg = "sys.argv[$1]";
                        }
                        
                        # remove the "$" sign from file/variable names
                        $arg =~ s/^\$//;
                        print " os.access($arg, os.R_OK)";
                    
                    # ELSE it is a normal string
                    } else {
                        print " os.access(\'$arg\', os.R_OK)";
                        
                    }
                    
                    $isR = 0;
                    
                    
                # ---------- "-d" ----------
                } elsif ( $isD eq 1 ){
                    # ---------- System Variable (e.g. $1, $2) ----------
                    if ( $arg =~ /^\$([0-9]+)/ ){
                        $arg = "sys.argv[$1]";
                        print " os.path.isdir($arg)";
                    } else {
                        print " os.path.isdir(\'$arg\')";
                    }
                    
                    
                    
                    $isD = 0;
                    
                    
                # ---------- comparison ----------
                } else {
                    
                    # ---------- variable type (e.g. $var) ----------
                    if ( $arg =~ /^\$\S+/ ){
                        
                        # ---------- System Variable (e.g. $1, $2) ----------
                        if ( $arg =~ /^\$([0-9]+)/ ){
                            print " sys.argv[$1]";
                        
                        # ---------- "$#" ----------
                        } elsif ( $arg =~ /^\$#/ ){
                            print " len(sys.argv[1\:])";
                            
                        # ---------- "$*" ----------
                        } elsif ( $arg =~ /^\$\*/ ) {
                            print " sys.argv[1:]";
                            
                        # ---------- "$$" ----------
                        } elsif ( $arg =~ /^\$\$/ ){
                            print " os.getpid()";
                        
                        # ---------- "other stuff" ---------
                        } elsif ( $arg =~ /\$([\@\?]{1})/ ){
                        
                        
                        # ---------- normal variable ----------
                        } else {
                            # remove the "$" sign from file/variable names
                            $arg =~ s/^\$//;
                            print " $arg";
                        }
                        
                    # ---------- integer type ----------
                    } elsif ( $arg =~ /^\-?\d+$/ ){
                        print " $arg";
                        
                    # ---------- comparison regexp ----------
                    } elsif ( $arg =~ /^\-le$/ ){
                        print " <=";
                        
                    } elsif ( $arg =~ /^\-ge$/ ){
                        print " >=";
                        
                    } elsif ( $arg =~ /^\-ne$/ ){
                        print " !=";
                        
                    } elsif ( $arg =~ /^\-lt$/ ){
                        print " <";
                        
                    } elsif ( $arg =~ /^\-gt$/ ){
                        print " >";
                        
                    } elsif ( $arg =~ /^\-eq$/ || $arg =~ /^\=$/ ){
                        print " ==";
                        
                    } elsif ( $arg =~ /^\-o$/ || $arg =~ /^\|\|$/ ){
                        print " or";
                        
                    } elsif ( $arg =~ /^\&\&$/ || $arg =~ /^\-a$/ ){
                        print " and";
                        
                    # ----------normal word ----------
                    } else {
                        
                        if ( $arg =~ /^ls$/){
                            print " subprocess.call(['ls'])";
                        } else {
                            #print in single quotes
                            print " \'$arg\'";
                        }
                    }
                }
                
                
            # ---------- "fgrep" ----------
            } elsif ( $isFgrep eq 1 ){
                
                # IF we "test -x $number $file" for file readability
                if ( $arg =~ /^\-x$/ ){
                    $isX = 1;
                    $count++;
                    next;
                    
                # ELSE IF we "test -q etc" for valid directory
                } elsif ( $arg =~ /^\-q$/ ){
                    $isQ = 1;
                    $count++;
                    next;
                }
                
                # IF we haven't already done so, print the start of the fgrep function
                if ( $fgrepPrint eq 0 ){
                    print " not subprocess.call(['fgrep'";
                    $fgrepPrint = 1;
                }
                
                if ( $isX eq 1 ){
                    print ", \'\-x\'";
                    $isX = 0;
                }
                
                if ( $isQ eq 1 ){
                    print ", \'\-q\'";
                    $isQ = 0;
                }
                
                # IF the arg is a variable with a "$" sign in front...
                if ( $arg =~ /^\$.*/ ){
                    
                    # remove the "$" sign from file/variable names
                    $arg =~ s/^\$//;
                    print ", str($arg)";
                    
                # ELSE it is a normal string
                } else {
                    print ", str($arg)";
                }
                
            }
            
            $count++;
        }

        # IF we were doing an Fgrep function, we close it off
        if ( $isFgrep eq 1 ){
            print "])";
        }

        print "\:";
        

    # ----------------- read -----------------
    } elsif ( $line =~ /^read *(\S+)/ ){
        
        print "$1 = sys.stdin.readline().rstrip()";
        
        
    # ----------------- remove file -----------------
    } elsif ( $line =~ /^rm *(.+)/ ){
        my $editType;
        my $tempVar;
        
        # IF we are removing something
        # fomat: rm -f $file
        if ( $1 =~ /\-(\w+) +\$(\S+)/ ){
            
            $editType = $1;
            $tempVar = $2;
            
            print "subprocess.call(\[\'rm\', \'\-$editType\', str($tempVar)\])";
        }
        
    # ----------------- exit -----------------
    } elsif ( $line =~ /^exit *(\S+)/ ) {
        
        
        print "sys.exit(0)";
        
    # ----------------- empty line -----------------
    } elsif ( $line =~ /^$/ ) {
        
    # ----------------- break -----------------
    } elsif ( $line =~ /^break$/ ) {
        print "break";
        
    # ----------------- IF we can't convert it, we comment it out. -----------------
    } else {
        
        if ( $line =~ /^\#/ ){
            print "$line";
        } else {
            # Lines we can't translate are turned into comments
            print "#$line";
        }
        

    }


    # ----------------- comments & new line -----------------
    if ( $line =~ /\S*( +\#.*)/ ){
        print "$1\n";
    } else {
        print "\n";
    }

}















